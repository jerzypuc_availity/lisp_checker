package availity.lisp.checker;

public class ValidateParenthesis {

    /**
     * We just count the valid parenthesis. No need to put it on stack as we do build tree walker here.
     * Checking if the parenthesis are in correct order "(" is followed by ")", not other way around
     * and checking that number of not-literal left and right parenthesis match.
     *
     * @param lispInput Lisp string to validate
     * @return true if parenthesis are properly nested and closed. False otherwise.
     */
    public static boolean validateParenthesis(final String lispInput) {
        if (lispInput == null) {
            return false;
        }

        int parenthesisCounter = 0; // if this goes below zero, return false. Add 1 for "(" and subtract 1 for ")"
        boolean hadAnyParenthesis = false; // check just in case string has no parenthesis at all and return false

        boolean inStringOrChar = false;
        boolean isComment = false;

        for (int i = 0; i < lispInput.length(); i++) {
            char charAt = lispInput.charAt(i);
            if (charAt == '\\') {
                i++; //skip next character - whatever it is its not what we are looking for
            } else if (charAt == '"') {
                inStringOrChar = !inStringOrChar; //flip it
            } else if (!inStringOrChar && charAt == ';') { // this is comment - ";" which is not part of the Lisp string
                isComment = true;
            } else if (charAt == '\n') {
                isComment = false;
            }
            if (!inStringOrChar && !isComment) {
                switch (charAt) {
                    case '(':
                        parenthesisCounter++;
                        hadAnyParenthesis = true;
                        break;
                    case ')':
                        parenthesisCounter--;
                        hadAnyParenthesis = true;
                        break;
                    default:
                        // do nothing
                        break;
                }
            }

            if (parenthesisCounter < 0) { // this means that we have closing parenthesis before opening... not good
                return false;
            }
        }
        return parenthesisCounter == 0 && hadAnyParenthesis; // if its not zero, we have more opening parenthesis than closing ones.
    }
}
