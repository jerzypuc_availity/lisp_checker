package availity.lisp.checker;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ValidateParenthesisTest {

    private static Stream<Arguments> lispStringsExpectations() {
        return Stream.of(
                Arguments.of(null, false),
                Arguments.of("", false),
                Arguments.of("abc", false),
                Arguments.of(")(", false),
                Arguments.of("())", false),
                Arguments.of("())(", false),
                Arguments.of("(()()\\())", false),
                Arguments.of("()(", false),
                Arguments.of("()", true),
                Arguments.of("()()()", true),
                Arguments.of("(((())))", true),
                Arguments.of("(((()())())())", true),

                // Lisp Strings
                Arguments.of("(write-line \"Mandatory Hello World :)\")", true),
                Arguments.of("(write-line \"Welcome to \\\"nice examples ;)\\\"\")", true),

                // Lisp Characters
                Arguments.of("(write (char= #\\a #\\)))", true),

                // comments
                Arguments.of("(;(((\n)", true),
                Arguments.of("(;(((\n;)", false)
        );
    }

    @ParameterizedTest(name = "Testing that \"{0}\" validation returns {1}")
    @MethodSource("lispStringsExpectations")
    void checkLispStrings(String lispString, boolean expectation) {
        assertEquals(expectation, ValidateParenthesis.validateParenthesis(lispString));
    }
}