# Availity Lisp parenthesis checker

Since I never had any experience with LISP, following are assumptions after small research about LISP.

1. string with no parenthesis returns false
2. number of "(" and ")" has to be equal except for the following
    - parenthesis inside the string do not count
    - parenthesis as character do not count
    - parenthesis inside comment do not count (from ";" to the "\n")

If we need to validate that the string is valid Lisp command this is definitely not good enough.

Limitations:
1. This implementation is not taking under consideration ‘#@count’ construct. [Lisp Comments](https://www.gnu.org/software/emacs/manual/html_node/elisp/Comments.html)
2. This implementation is not checking if the given string is valid Lisp command. This allows for string like "some()" to return true. 
3. This implementation is not checking for proper positioning, just nesting and closing of parenthesis.

To execute all tests, run
```
mvn clean test
```

